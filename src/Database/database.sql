CREATE DATABASE IF NOT EXISTS calendar;
CREATE USER 'calendar'@'%' IDENTIFIED WITH mysql_native_password BY 'calendar';
GRANT ALL PRIVILEGES ON calendar . * TO 'calendar'@'%';
FLUSH PRIVILEGES;
CREATE TABLE IF NOT EXISTS calendar.events(
    id varchar(255) not null,
    subject varchar(255)not null,
    start_date datetime not null,
    end_date datetime not null,
    res_code varchar(255) not null,
    ver_code varchar(255)not null,
    PRIMARY KEY(id)
);