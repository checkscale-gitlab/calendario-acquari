const moment = require('moment')
const fs = require('fs')

module.exports = {
    
    log:function(path,logLevel,message,date){
        fs.appendFile(path,'['+logLevel+']['+date+']:'+message+'\n', (err,content) => {
            if (err) console.error(err)
        })
    },
    
    logNow:function(path,logLevel,message){
        let date = moment(new Date()).format("YYYY-MM-DD HH:mm")
        this.log(path,logLevel,message,date)
    },
    
    logError:function(path,message){
        this.logNow(path,"ERROR",message)
    },
    
    logInfo:function(path,message){
        this.logNow(path,"INFO",message)
    }
}
